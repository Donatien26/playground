import { AxiosInstance } from 'axios';
import getClient from './axios';

export const getCocktailByIngredients = (search: String) =>
    getClient()
        .then((client: AxiosInstance) =>
            client
                .get('/filter.php', {
                    params: {
                        i: search,
                    },
                })
                .then((resp: { data: { drinks: any; }; }) => resp.data.drinks)
        )

export const getCocktailName = (search: String) =>
    getClient()
        .then((client: AxiosInstance) =>
            client
                .get('/search.php', {
                    params: {
                        s: search,
                    },
                })
                .then((resp: { data: { drinks: any; }; }) => resp.data.drinks)
        )



