import axios from 'axios';

const configFilePath = `${window.location.origin}/configuration.json`;


export const getConfigFile = () => {
    let client = axios.create({
        baseURL: configFilePath, headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'Access-Control-Allow-Origin': '*',
        }
    })
    return client.get("").then(resp => resp.data).catch(err => console.log("Impossible de récuperer la configuration"))
}

export const getConfig = (property: string) => getConfigFile().then(resp => resp[property]).catch(err => console.log("la property " + property + " n'existe pas dans la réponse"))

export const configWrapper = (service: any) =>
    getConfigFile().then(resp => service(resp));
