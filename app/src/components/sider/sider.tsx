import { DesktopOutlined, FileOutlined, HomeOutlined, TeamOutlined, UserOutlined } from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
const { SubMenu } = Menu;
const { Sider } = Layout;

const MySider = () => {
    const [collapsed, setCollapsed] = useState(false)
    const history = useHistory()
    
    const onCollapse = () => {
        setCollapsed(!collapsed)
    }

    const handleClick = (path: string) => history.push(path)

    return (
        <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
            <div className="logo" />
            <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
                <Menu.Item key="1" icon={<HomeOutlined />} onClick={() => handleClick("/")}>
                    Home
                </Menu.Item>
                <Menu.Item key="2" icon={<DesktopOutlined />}>
                    Option 2
                </Menu.Item>
                <SubMenu key="sub1" icon={<UserOutlined />} title="User">
                    <Menu.Item key="3">Tom</Menu.Item>
                    <Menu.Item key="4">Bill</Menu.Item>
                    <Menu.Item key="5">Alex</Menu.Item>
                </SubMenu>
                <SubMenu key="sub2" icon={<TeamOutlined />} title="Team">
                    <Menu.Item key="6">Team 1</Menu.Item>
                    <Menu.Item key="8">Team 2</Menu.Item>
                </SubMenu>
                <Menu.Item key="9" icon={<FileOutlined />} />
            </Menu>
        </Sider >
    )
}
export default MySider