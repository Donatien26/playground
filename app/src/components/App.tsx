import { BackTop, Layout, Typography } from 'antd';
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Sider from '../components/sider/sider';
import './App.scss';
import Cocktail from './content/cocktail/cocktail';
import Home from './content/home/home';

const { Header, Content, Footer } = Layout;
const { Title } = Typography;

const App = () => {
	return (
		<Router>
			<Layout style={{ minHeight: '100vh' }}>
				<Sider />
				<Layout className="site-layout">
					<Header
						className="site-header"
						style={{
							padding: 0,
							background: '#001529',
						}}
					>
						<Title style={{ color: '#ffffff' }}>
							Playground
						</Title>
					</Header>
					<Content style={{ margin: '0 16px' }}>
						<Switch>
							<Route
								exact
								path="/cocktail/:id"
								component={Cocktail}
							/>
							<Route path="/" component={Home} />
						</Switch>
						<BackTop />
					</Content>
					<Footer style={{ textAlign: 'center' }}>
						Ant Design ©2018 Created by Donatien ENEMAN
					</Footer>
				</Layout>
			</Layout>
		</Router>
	);
};

export default App;
