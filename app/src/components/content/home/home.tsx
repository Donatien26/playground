import { SearchOutlined } from '@ant-design/icons';
import { Card, Col, Radio, Row, Spin } from 'antd';
import Search from 'antd/lib/input/Search';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
	getCocktailByIngredients,
	getCocktailName,
} from '../../../configuration/api';
import '../../App.scss';

const { Meta } = Card;

interface cocktail {}

const Home = () => {
	const [search, setSearch] = useState<null | string>(null);
	const [results, setResults] = useState<null | cocktail[]>(null);
	const [loading, setLoading] = useState(false);
	const history = useHistory();
	const [choice, setChoice] = useState('ingredient');
	const handleClick = (path: string) => history.push(path);

	useEffect(() => {
		if (search) {
			setLoading(true);
			if (choice === 'ingredients') {
				getCocktailByIngredients(search).then((cocktails) => {
					setResults(cocktails);
					setLoading(false);
				});
			}
			if (choice === 'name') {
				getCocktailName(search).then((cocktails) => {
					setResults(cocktails);
					setLoading(false);
				});
			}
		}
	}, [search, choice]);

	return (
		<>
			<Search
				placeholder="input search text"
				enterButton="Search"
				size="large"
				onSearch={(value) => setSearch(value)}
			/>
			<Radio.Group
				onChange={(e) => setChoice(e.target.value)}
				value={choice}
			>
				<Radio value={'ingredients'}>ingrédients</Radio>
				<Radio value={'name'}>name</Radio>
			</Radio.Group>
			{loading ? (
				<div className="loader">
					<Spin />
				</div>
			) : (
				<Row gutter={16}>
					{results ? (
						results.map((cocktail: any) => {
							return (
								<Col
									className="gutter-row"
									span={6}
								>
									<Card
										hoverable
										cover={
											<img
												alt="example"
												src={
													cocktail.strDrinkThumb
												}
											/>
										}
										actions={[
											<SearchOutlined
												key="detail"
												onClick={() =>
													handleClick(
														'/cocktail/' +
															cocktail.idDrink,
													)
												}
											/>,
										]}
									>
										<Meta
											title={
												cocktail.strDrink
											}
										/>
									</Card>
								</Col>
							);
						})
					) : (
						<></>
					)}
				</Row>
			)}
		</>
	);
};

export default Home;
