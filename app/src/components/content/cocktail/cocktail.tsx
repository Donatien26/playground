import { Col, Descriptions, Image, Row, Typography } from 'antd';
import { AxiosInstance } from 'axios';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import getClient from '../../../configuration/axios';

const { Title } = Typography;

const Cocktail = () => {
	const { id } = useParams<any>();
	const [cocktail, setCocktail] = useState<null | any>(null);

	useEffect(() => {
		getClient()
			.then((client: AxiosInstance) =>
				client
					.get('/lookup.php', { params: { i: id } })
					.then((resp) => resp.data.drinks[0])
					.catch((err) => console.log(err)),
			)
			.then((cocktail) => setCocktail(cocktail));
	}, [id]);

	return cocktail ? (
		<Typography>
			<Row justify="space-between" align="bottom">
				<Col span={6}>
					<Title>{cocktail.strDrink}</Title>
					<Image width={200} src={cocktail.strDrinkThumb} />
				</Col>
				<Col span={18}>
					<Descriptions
						title="Cocktail Info"
						layout="horizontal"
						column={2}
					>
						<Descriptions.Item label="Name">
							{cocktail.strDrink}
						</Descriptions.Item>
						<Descriptions.Item label="Instruction">
							{cocktail.strInstructions}
						</Descriptions.Item>
					</Descriptions>
					,
				</Col>
			</Row>
		</Typography>
	) : (
		<></>
	);
};

export default Cocktail;
